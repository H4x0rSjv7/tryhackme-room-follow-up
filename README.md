Source : https://tryhackme.com/resources/blog/free_path

👍 = Completed.

👎 = Archived.

⏳ = In progress.

🔴 = Stopped, depends on other tasks, depends on knowledge from other rooms.

Progression :

\{ =======================>________ \}
 - Total = 165.
 - Completed = 121.

# Level 1 - Intro
- [👍] OpenVPN https://tryhackme.com/room/openvpn
- [👍] Django https://tryhackme.com/room/django
- [👍] Welcome https://tryhackme.com/jr/welcome
- [👍] Intro to Researching https://tryhackme.com/room/introtoresearch
- [👍] The Hacker Methodology https://tryhackme.com/room/hackermethodology
- [👍] Learn Linux https://tryhackme.com/module/linux-fundamentals
- [👍] Crash Course Pentesting https://tryhackme.com/room/ccpentesting

**Introductory CTFs to get your feet wet**

- [👍] Google Dorking https://tryhackme.com/room/googledorking
- [👍] OHsint https://tryhackme.com/room/ohsint
- [👍] Shodan.io https://tryhackme.com/room/shodan

# Level 2 - Tooling
- [👍] Tmux https://tryhackme.com/room/rptmux
- [👍] Nmap https://tryhackme.com/room/furthernmap
- [👍] Web Scanning https://tryhackme.com/room/rpwebscanning
- [👍] Sublist3r https://tryhackme.com/room/rpsublist3r
- [👍] Metasploit https://tryhackme.com/room/rpmetasploit
- [👍] Hydra https://tryhackme.com/room/hydra
- [👍] Linux Privesc https://tryhackme.com/room/linuxprivesc
- [👍] Web Scanning https://tryhackme.com/room/rpwebscanning
- [👍] Shodan https://tryhackme.com/room/shodan
- [👍] RustScan (I invented RustScan so excuse the self-promo) https://tryhackme.com/room/rustscan
- [👍] Rust https://tryhackme.com/room/rust

**More introductory CTFs**
- [👍] Vulnversity - https://tryhackme.com/room/vulnversity
- [👍] Blue - https://tryhackme.com/room/blue
- [👍] Simple CTF https://tryhackme.com/room/easyctf
- [👍] Bounty Hacker https://tryhackme.com/room/cowboyhacker
- [👍] Brute It https://tryhackme.com/room/bruteit

# Level 3 - Crypto & Hashes with CTF practice
- [👍] Crack the hash https://tryhackme.com/room/crackthehash
- [👍] Agent Sudo https://tryhackme.com/room/agentsudoctf
- [👍] The Cod Caper https://tryhackme.com/room/thecodcaper
- [👍] Ice https://tryhackme.com/room/ice
- [👍] Lazy Admin https://tryhackme.com/room/lazyadmin
- [👍] Basic Pentesting https://tryhackme.com/room/basicpentestingjt

# Level 4 - Web
- [👍] OWASP top 10 https://tryhackme.com/room/owasptop10
- [ ] Inclusion https://tryhackme.com/room/inclusion
- [ ] Injection https://tryhackme.com/room/injection
- [👍] Vulnversity https://tryhackme.com/room/vulnversity
- [👍] Basic Pentesting https://tryhackme.com/room/basicpentestingjt
- [👍] Juiceshop https://tryhackme.com/room/owaspjuiceshop
- [ ] Ignite https://tryhackme.com/room/ignite
- [ ] Overpass https://tryhackme.com/room/overpass
- [ ] Year of the Rabbit https://tryhackme.com/room/yearoftherabbit
- [ ] DevelPy https://tryhackme.com/room/bsidesgtdevelpy
- [ ] Jack of all trades https://tryhackme.com/room/jackofalltrades
- [ ] Bolt https://tryhackme.com/room/bolt

# Level 5 - Reverse Engineering
- [👎] Intro to x86 64 https://tryhackme.com/room/introtox8664
- [👎] CC Ghidra https://tryhackme.com/room/ccghidra
- [👎] CC Radare2 https://tryhackme.com/room/ccradare2
- [👎] CC Steganography https://tryhackme.com/room/ccstego
- [👎] Reverse Engineering https://tryhackme.com/room/reverseengineering
- [ ] Reversing ELF https://tryhackme.com/room/reverselfiles
- [ ] Dumping Router Firmware https://tryhackme.com/room/rfirmware
## Alternative
Source : https://tryhackme.com/forum/thread/628c68d58452fe005195ee5a

- [ ] https://tryhackme.com/room/introtopwntools
- [ ] https://tryhackme.com/room/win64assembly
- [ ] https://tryhackme.com/room/windowsreversingintro

# Level 6 - Networking
- [👍] Introduction to Networking https://tryhackme.com/room/introtonetworking
- [ ] Smag Grotto https://tryhackme.com/room/smaggrotto
- [👍] Overpass 2 https://tryhackme.com/room/overpass2hacked

# Level 7 - PrivEsc
- [ ] Sudo Security Bypass https://tryhackme.com/room/sudovulnsbypass
- [ ] Sudo Buffer Overflow https://tryhackme.com/room/sudovulnsbof
- [ ] Windows Privesc Arena https://tryhackme.com/room/windowsprivescarena
- [ ] Linux Privesc Arena https://tryhackme.com/room/linuxprivescarena
- [ ] Windows Privesc https://tryhackme.com/room/windows10privesc
- [ ] Blaster https://tryhackme.com/room/blaster
- [ ] Ignite https://tryhackme.com/room/ignite
- [👍] Kenobi https://tryhackme.com/room/kenobi
- [ ] Capture the flag https://tryhackme.com/room/c4ptur3th3fl4g
- [👍] Pickle Rick https://tryhackme.com/room/picklerick

# Level 8 - CTF practice
- [👍] Post Exploitation Basics https://tryhackme.com/room/postexploit
- [ ] Inclusion https://tryhackme.com/room/inclusion
- [ ] Dogcat https://tryhackme.com/room/dogcat
- [ ] LFI basics https://tryhackme.com/room/lfibasics
- [ ] Buffer Overflow Prep https://tryhackme.com/room/bufferoverflowprep
- [ ] Overpass https://tryhackme.com/room/overpass
- [ ] Break out the cage https://tryhackme.com/room/breakoutthecage1
- [ ] Lian Yu https://tryhackme.com/room/lianyu

# Level 9 - Windows
- [👍] Attacktive Directory https://tryhackme.com/room/attacktivedirectory
- [ ] Retro https://tryhackme.com/room/retro
- [ ] Blue Print https://tryhackme.com/room/blueprint
- [ ] Anthem https://tryhackme.com/room/anthem
- [ ] Relevant https://tryhackme.com/room/relevant

The following is a list about free rooms in current learning paths.
This list is ordered from least to most percentage completed from the leaning path (this include paid rooms).

# Pre Security
https://tryhackme.com/path-action/presecurity/join

- [👍] Learning Cyber Security https://tryhackme.com/room/beginnerpathintro
- [👍] What is Networking? https://tryhackme.com/room/whatisnetworking
- [👍] Intro to LAN https://tryhackme.com/room/introtolan
- [👍] DNS in Detail https://tryhackme.com/room/dnsindetail
- [👍] HTTP in detail https://tryhackme.com/room/httpindetail
- [👍] Linux Fundamentals Part 1 https://tryhackme.com/room/linuxfundamentalspart1
- [👍] Linux Fundamentals Part 2 https://tryhackme.com/room/linuxfundamentalspart2
- [👍] Linux Fundamentals Part 3 https://tryhackme.com/room/linuxfundamentalspart3
- [👍] Windows Fundamentals 1 https://tryhackme.com/room/windowsfundamentals1xbx
- [👍] Windows Fundamentals 2 https://tryhackme.com/room/windowsfundamentals2x0x
- [👍] Windows Fundamentals 3 https://tryhackme.com/room/windowsfundamentals3xzx

# CompTIA Pentest+
https://tryhackme.com/path-action/pentestplus/join

- [👍] Tutorial https://tryhackme.com/room/tutorial
- [👍] Nmap https://tryhackme.com/room/furthernmap
- [👍] Metasploit https://tryhackme.com/room/rpmetasploit
- [👍] Nessus https://tryhackme.com/room/rpnessusredux
- [👍] Hydra https://tryhackme.com/room/hydra
- [👍] Web Fundamentals https://tryhackme.com/room/webfundamentals
- [👍] OWASP Top 10 https://tryhackme.com/room/owasptop10
- [👍] OWASP Juice Shop https://tryhackme.com/room/owaspjuiceshop
- [👍] Vulnversity https://tryhackme.com/room/vulnversity
- [👍] Introductory Networking https://tryhackme.com/room/introtonetworking
- [👍] Kenobi https://tryhackme.com/room/kenobi
- [👍] Attacktive Directory https://tryhackme.com/room/attacktivedirectory
- [👍] Post-Exploitation Basics https://tryhackme.com/room/postexploit

# Complete Beginner
https://tryhackme.com/path-action/beginner/join

- [👍] Tutorial https://tryhackme.com/room/tutorial
- [👍] Starting Out In Cyber Sec https://tryhackme.com/room/startingoutincybersec
- [👍] Introductory Researching https://tryhackme.com/room/introtoresearch
- [👍] Linux Fundamentals Part 1 https://tryhackme.com/room/linuxfundamentalspart1
- [👍] Linux Fundamentals Part 2 https://tryhackme.com/room/linuxfundamentalspart2
- [👍] Linux Fundamentals Part 3 https://tryhackme.com/room/linuxfundamentalspart3
- [👍] Introductory Networking https://tryhackme.com/room/introtonetworking
- [👍] Nmap https://tryhackme.com/room/furthernmap
- [👍] Web Fundamentals https://tryhackme.com/room/webfundamentals
- [👍] OWASP Top 10 https://tryhackme.com/room/owasptop10
- [👍] OWASP Juice Shop https://tryhackme.com/room/owaspjuiceshop
- [👍] Pickle Rick https://tryhackme.com/room/picklerick
- [👍] Encryption - Crypto 101 https://tryhackme.com/room/encryptioncrypto101
- [👍] Windows Fundamentals 1 https://tryhackme.com/room/windowsfundamentals1xbx
- [👍] Windows Fundamentals 2 https://tryhackme.com/room/windowsfundamentals2x0x
- [👍] Metasploit https://tryhackme.com/room/rpmetasploit
- [👍] Blue https://tryhackme.com/room/blue
- [👍] Linux PrivEsc https://tryhackme.com/room/linuxprivesc
- [👍] Vulnversity https://tryhackme.com/room/vulnversity
- [👍] Basic Pentesting https://tryhackme.com/room/basicpentestingjt
- [👍] Kenobi https://tryhackme.com/room/kenobi

# Cyber Defense
https://tryhackme.com/paths

- [👍] Tutorial https://tryhackme.com/room/tutorial
- [👍] Introductory Networking https://tryhackme.com/room/introtonetworking
- [👍] Nessus https://tryhackme.com/room/rpnessusredux
- [👍] MITRE https://tryhackme.com/room/mitre
- [👍] Yara https://tryhackme.com/room/yara
- [👍] OpenVAS https://tryhackme.com/room/openvas

# Offensive Pentesting
https://tryhackme.com/path-action/pentesting/join

- [👍] Tutorial https://tryhackme.com/room/tutorial
- [👍] Vulnversity https://tryhackme.com/room/vulnversity
- [👍] Blue https://tryhackme.com/room/blue
- [👍] Kenobi https://tryhackme.com/room/kenobi
- [👍] Daily Bugle https://tryhackme.com/room/dailybugle
- [👍] Overpass 2 - Hacked https://tryhackme.com/room/overpass2hacked
- [ ] Relevant https://tryhackme.com/room/relevant
- [⏳] Internal https://tryhackme.com/room/internal
- [ ] Buffer Overflow Prep https://tryhackme.com/room/bufferoverflowprep
- [ ] Gatekeeper https://tryhackme.com/room/gatekeeper
- [ ] Brainpan 1 https://tryhackme.com/room/brainpan
- [👍] Attacktive Directory https://tryhackme.com/room/attacktivedirectory
- [👍] Post-Exploitation Basics https://tryhackme.com/room/postexploit
- [ ] Mr Robot CTF https://tryhackme.com/room/mrrobot
- [ ] Retro https://tryhackme.com/room/retro

# Web Fundamentals
https://tryhackme.com/path-action/web/join

- [👍] Tutorial https://tryhackme.com/room/tutorial
- [👍] Introductory Networking https://tryhackme.com/room/introtonetworking
- [👍] Web Fundamentals https://tryhackme.com/room/webfundamentals
- [👍] Introduction to Django https://tryhackme.com/room/django
- [👍] Burp Suite: The Basics https://tryhackme.com/room/burpsuitebasics
- [👍] Burp Suite: Repeater https://tryhackme.com/room/burpsuiterepeater
- [👍] Introduction to OWASP ZAP https://tryhackme.com/room/learnowaspzap
- [👍] Pickle Rick https://tryhackme.com/room/picklerick
- [👍] OWASP Juice Shop https://tryhackme.com/room/owaspjuiceshop

# Jr Penetration Tester
https://tryhackme.com/path-action/jrpenetrationtester/join

- [👍] Pentesting Fundamentals https://tryhackme.com/room/pentestingfundamentals
- [👍] Principles of Security https://tryhackme.com/room/principlesofsecurity
- [👍] Walking An Application https://tryhackme.com/room/walkinganapplication
- [👍] Content Discovery https://tryhackme.com/room/contentdiscovery
- [👍] SQL Injection https://tryhackme.com/room/sqlinjectionlm
- [👍] Burp Suite: The Basics https://tryhackme.com/room/burpsuitebasics
- [👍] Burp Suite: Repeater https://tryhackme.com/room/burpsuiterepeater
- [👍] Passive Reconnaissance https://tryhackme.com/room/passiverecon
- [👍] Active Reconnaissance https://tryhackme.com/room/activerecon
- [👍] Nmap Live Host Discovery https://tryhackme.com/room/nmap01
- [👍] Vulnerabilities 101 https://tryhackme.com/room/vulnerabilities101
- [👍] Metasploit: Introduction https://tryhackme.com/room/metasploitintro
- [👍] Linux PrivEsc https://tryhackme.com/room/linprivesc
